# Plataforma Ensenya el Cor
**1. Sobre la webapp:**
Aquesta webapp té com a funció principal la recollida d’informació d’empreses i entitats en diferents dimensions de valor per a calcular diferents indicadors de funcionament i estat d’aquestes empreses i entitats.
 
En la seva versió original, aquesta webapp té dos itineraris per a fer aquesta recollida de dades: el balanç social i el balanç social bàsic. Des d’aquest itineraris els usuaris i usuàries poden introduir diferents dades relatives al funcionament de la organització de la que formen part i, a partir del càlcul dels indicadors associats a cada itinerari, obtenir un informe generat de manera automàtica amb el recull de tots les indicadors -quantitatius i qualitatius- associats a cada un dels itineraris.
 
A més d’aquesta funció cor, l’eina original disposa també d’altres funcions complementàries:
    • Obtenció del segell de balanç social i establiment de compromisos de millora anuals.
    • Enviament de qüestionaris de qualitat a quatre grups d’interès: persones treballadores; persones o organitzacions clientes o usuàries; persones voluntàries; i persones sòcies.
    • Adjunció de documents.
    • Backoffice amb múltiples funcions:
        ◦ Edició de tots els camps (mòduls, indicadors, preguntes, etc.) que composen els itineraris de balanç social i balanç social bàsic.
        ◦ Possibilitat de crear tots els itineraris nous que es vulgui.
        ◦ Funcions d’exportació de dades a partir de diferents vectors.
        ◦ Funcions de seguiment a les empreses i entitats usuàries de qualsevol dels itineraris.
 
Aquesta webapp està al servei d’un propòsit general establert per la Xarxa d’Economia Solidària: afavorir una progressiva transformació de les empreses, entitats i l’economia en general, cap a formes de fer més democràtiques, igualitàries, sostenibles i dignes.
 
**2. Llibreries i recursos Open Source utilitzades:**
Per a la programació de la webapp s’han utilitzat llibreries Spring, Vaadin i Eclipse, i el Jamgo Framework. 
 
**3. Titularitat de la webapp (copyright):**
Aquesta webapp ha estat desenvolupada per l’associació Xarxa d’Economia Solidària a partir d’ara XES), mitjançant els serveis tecnològics de Jamgo SCCL (a partir d’ara Jamgo). La XES ha encarregat el desenvolupament informàtic a Jamgo mitjançant una contractació per prestació de serveis. Quant a l’autoria de la webapp, se li poden atribuir les següents aportacions a cada una d’elles:
 
La XES va crear la primera versió del balanç social a l’any 2007. Des de llavors, any rere any, a través de la comissió de balanç social i de les aportacions voluntàries de les diferents organitzacions associades a la XES, s’ha anat millorant i consolidant el procediment i plantejament del balanç social com a instrument de rendició de comptes. Per tant, la concepció del balanç social com a eina és una obra col·lectiva emmarcada en la feina de la XES i coordinada per aquesta. Pel que fa més concretament a la pròpia webapp allotjada a www.ensenyaelcor.org, la XES ha contribuït definint el gruix de funcionalitats de l’eina, així com el workflow o experiència d’usuari d’acord amb tota l’experiència adquirida amb els anys. 
 
Per la seva part, Jamgo, d’acord a la petició feta per la XES, ha programat la webapp a partir de les llibreries Vaadin, Spring i Eclipse, i el Jamgo Framework, fent una programació adaptada i específica d’acord a les necessitats del projecte balanç social. Els recursos utilitzats per Jamgo estan publicats sota la mateixa llicència AGPL, de manera que són compatibles amb les condicions de distribució de la webapp.  
 
La titularitat (copyright) de la webapp és de la Xarxa d’Economia Solidària com a entitat coordinadora de la obra col·lectiva.  
 
